Assignment 1 - Expected results

How many times a month do you throw out food? In sweden a highly talked-about subject is “matsvinn”, which alludes that we throw out way too
much food. One reason is because we simply cannot remember what products we have in our home and when they meet their expiration date. This is
just one of many problems our application will help solve.

Let us say that you know exactly what groceries you have. You also know when they expire and can therefore cook a meal using the ingredients
about to meet their deadline. Would that not be great? Well, that is exactly what the soon created application will allow you to do. Not only
that but it will also help you get rid of the boring task of creating a shopping-list whenever you go to the grocery store. 

The application will present a tableview showing different food categories for your fridge. It is in these categories you will store your
different groceries. Each category also has a number, specifying the number of old groceries in that category. At the bottom of the screen you
can navigate via a tab bar between fridge (default), freezer and pantry. When the user has clicked on a specificcategory and is presented 
with the various produce for that category. The cells will specify how many days until that product has expired.

The freezer and the pantry will have their own different categories. In the top right corner you will be sent to a new view where you can
specify the name of the product, its expiration date and which category it will placed in. After you have
specified everything you can either instantly add another product or decide that you are done. If your choice is the latter you are presented a
list of all the newly added items where you can go through them and edit them if necessary. 

In the tab bar mentioned above there is a fourth alternative, settings. Here the user will be able to personalize the application to his or hers
own needs. Another feature in this tab will be to generate a shopping-list, which is the functionality mentioned earlier. 