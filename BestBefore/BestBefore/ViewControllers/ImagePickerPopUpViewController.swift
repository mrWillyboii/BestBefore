//
//  PopUpViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-11-09.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class ImagePickerPopUpViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var popUpWindowView: UIView!
    @IBOutlet weak var imageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var popUpImageView: UIImageView!
    
    var pickedImage: UIImage = UIImage()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        popUpWindowView.layer.shadowColor = UIColor.gray.cgColor
        popUpWindowView.layer.shadowOffset = CGSize(width: 0, height: 2)
        popUpWindowView.layer.shadowOpacity = 0.2
        popUpWindowView.layer.shadowRadius = 1.0
        
        popUpWindowView.layer.cornerRadius = 5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        
        if touch?.view != popUpWindowView {
            self.dismiss(animated: true, completion: nil);
        }
    }

    @IBAction func takePicture(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    @IBAction func openPhotoLibrary(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        pickedImage = image
        picker.dismiss(animated: true, completion: nil)
        returnImage()
    }
    
    func returnImage(){
        let tabBar = presentingViewController as? UITabBarController
        let presentingVC = presentingViewController?.childViewControllers[(tabBar?.selectedIndex)!].childViewControllers
        let topChild = presentingVC![(presentingVC?.count)! - 1]
        if topChild is AddCategoryViewController{
            (topChild as? AddCategoryViewController)?.setImageView(image: pickedImage)
        }else if topChild is ShowDetailViewController{
            (topChild as? ShowDetailViewController)?.setImageView(image: pickedImage)
        }else if topChild is AddViewController{
            (topChild as? AddViewController)?.setImageView(image: pickedImage)
        }else if topChild is ShowCategoryDetailViewController {
            (topChild as? ShowCategoryDetailViewController)?.setImageView(image: pickedImage)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
}
