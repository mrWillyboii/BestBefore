//
//  AddCategoryViewController.swift
//  BestBefore
//
//  Created by Willy Larsson on 2017-11-14.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import CoreData
import DLRadioButton

class AddCategoryViewController: UIViewController {

    @IBOutlet weak var detailView: CategoryDetailView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    private let oldConstraintConstant: CGFloat = 16.0
    
    let coreDataAccess : CategoriesCoreDataAccess = CategoriesCoreDataAccess()
    var categoryId : NSManagedObjectID = NSManagedObjectID()
    var flag : String = String()
    var cameFromAddGrocery: Bool = false
    var doneButtonPressed : Bool = false
    var storage: Int16 = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        if cameFromAddGrocery {
            detailView.radioButtonStackView.isUserInteractionEnabled = false
            detailView.radioButtonStackView.isHidden = true
        }
        
        detailView.layer.shadowColor = UIColor.gray.cgColor
        detailView.layer.shadowOffset = CGSize(width: 0, height: 2)
        detailView.layer.shadowOpacity = 0.2
        detailView.layer.shadowRadius = 1.0
        
        setStorageRadioButton()
        
        let addImageGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.addImage(_:)))
        detailView.categoryImageView.isUserInteractionEnabled = true
        detailView.categoryImageView.addGestureRecognizer(addImageGestureRecognizer)
        detailView.categoryImageView.image = #imageLiteral(resourceName: "apple-1")
    }
    
    func setStorageRadioButton(){
        switch tabBarController?.selectedIndex {
        case 0?:
            detailView.fridgeRadioButton.sendActions(for: .touchUpInside)
        case 1?:
            detailView.freezerRadioButton.sendActions(for: .touchUpInside)
        case 2?:
            detailView.pantryRadioButton.sendActions(for: .touchUpInside)
        default:
            break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.extendedLayoutIncludesOpaqueBars = true
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isOpaque = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                bottomConstraint.constant = keyboardSize.height
                view.setNeedsLayout()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let userInfo = notification.userInfo{
            if ((userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
                bottomConstraint.constant = oldConstraintConstant
                view.setNeedsLayout()
            }
        }
    }
    
    @objc func addImage(_ sender: UITapGestureRecognizer){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = storyboard.instantiateViewController(withIdentifier: "PopUpViewControllerId") as! ImagePickerPopUpViewController
        
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        popupVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(popupVC, animated: true, completion: nil)
    }
    
    @IBAction func doneButtonClicked(_ sender: Any) {
        doneButtonPressed = true
        if(checkIfTextfieldIsEmpty()) {
            return
        }
        
        if cameFromAddGrocery {
            coreDataAccess.editCategory(name: detailView.nameTextField.text!, image: detailView.categoryImageView.image!, categoryId: categoryId, storage: storage)
        } else {
            coreDataAccess.addCategory(name: detailView.nameTextField.text!, storage: detailView.getSelectedStorage(), image: detailView.categoryImageView.image!)
            _ = navigationController?.popToRootViewController(animated: true)
        }
        
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isOpaque = false
        
        if(flag == "done") {
            _ = navigationController?.popToRootViewController(animated: true)
        }
        else if (flag == "add more") {
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    func checkIfTextfieldIsEmpty() -> Bool {
        if(detailView.nameTextField.text?.isEmpty)! {
            detailView.nameTextField.layer.borderWidth = 1.0
            detailView.nameTextField.layer.borderColor = UIColor.red.cgColor
            return true
        }
        else {
            detailView.nameTextField.layer.borderWidth = 0.0
            detailView.nameTextField.layer.borderColor = UIColor.black.cgColor
            return false
        }
    }
    
    func setImageView(image: UIImage){
        detailView.categoryImageView.image = image
    }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isOpaque = false
        NotificationCenter.default.removeObserver(self)
        if cameFromAddGrocery && !doneButtonPressed {
            coreDataAccess.deleteCategory(categoryId: categoryId)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
