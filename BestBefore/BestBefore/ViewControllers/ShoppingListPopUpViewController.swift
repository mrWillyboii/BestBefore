//
//  ShoppingListPopUpViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-05.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class ShoppingListPopUpViewController: UIViewController {

    @IBOutlet weak var popUpWindowView: UIView!
    @IBOutlet weak var infoText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        popUpWindowView.layer.shadowColor = UIColor.gray.cgColor
        popUpWindowView.layer.shadowOffset = CGSize(width: 0, height: 2)
        popUpWindowView.layer.shadowOpacity = 0.2
        popUpWindowView.layer.shadowRadius = 1.0
        popUpWindowView.layer.cornerRadius = 5
        
        
        if UIScreen.main.bounds.width <= 320.0 {
            infoText.font = UIFont(name: (infoText.font?.fontName)!, size: 10)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        
        if touch?.view != popUpWindowView {
            self.dismiss(animated: true, completion: nil);
        }
    }
    
    @IBAction func yesButtonClicked(_ sender: Any) {
        let generatedListVC = presentingViewController?.childViewControllers[3].childViewControllers.last as! GeneratedListViewController
        
        self.dismiss(animated: true) {
            generatedListVC.createBoughtGroceriesList()
        }
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
}
