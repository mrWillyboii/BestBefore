//
//  ShowCategoryDetailViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-11.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import CoreData

class ShowCategoryDetailViewController: UIViewController {

    @IBOutlet weak var detailView: CategoryDetailView!
    let categoriesCoreDataAccess : CategoriesCoreDataAccess = CategoriesCoreDataAccess()
    var category: NSManagedObject = NSManagedObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = editButtonItem
        
        initDetails(category)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.extendedLayoutIncludesOpaqueBars = true
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isOpaque = true
    }
    
    func initDetails(_ category: NSManagedObject){
        let tabBar = self.tabBarController as! UITabBarController
        
        detailView.fillView(category, (Int16(tabBar.selectedIndex)))
        detailView.layer.shadowColor = UIColor.gray.cgColor
        detailView.layer.shadowOffset = CGSize(width: 0, height: 2)
        detailView.layer.shadowOpacity = 0.2
        detailView.layer.shadowRadius = 1.0
    }

    override func setEditing(_ editing: Bool, animated: Bool) {
        let barButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.onDoneButtonPressed(_:)))
        let addImageGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.addImage(_:)))
        detailView.categoryImageView.isUserInteractionEnabled = true
        detailView.categoryImageView.addGestureRecognizer(addImageGestureRecognizer)
        navigationItem.rightBarButtonItem = barButton
        detailView.makeEditable()
    }
    
    @objc func onDoneButtonPressed(_ sender: Any) {
        if(checkIfTextfieldIsEmpty()) {
            return
        }
        
        categoriesCoreDataAccess.editCategory(name: detailView.nameTextField.text!, image: detailView.categoryImageView.image!, categoryId: category.objectID, storage: detailView.getSelectedStorage())
        
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    func checkIfTextfieldIsEmpty() -> Bool {
        if(detailView.nameTextField.text?.isEmpty)! {
            detailView.nameTextField.layer.borderWidth = 1.0
            detailView.nameTextField.layer.borderColor = UIColor.red.cgColor
            return true
        }
        else {
            detailView.nameTextField.layer.borderWidth = 0.0
            detailView.nameTextField.layer.borderColor = UIColor.black.cgColor
            return false
        }
    }
    
    @objc func addImage(_ sender: UITapGestureRecognizer){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = storyboard.instantiateViewController(withIdentifier: "PopUpViewControllerId") as! ImagePickerPopUpViewController
        
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        popupVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(popupVC, animated: true, completion: nil)
    }
    
    func setImageView(image: UIImage){
        detailView.categoryImageView.image = image
    }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isOpaque = false
    }
}
