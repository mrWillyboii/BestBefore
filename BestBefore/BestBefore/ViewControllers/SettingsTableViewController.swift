//
//  SettingsTableViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-11-13.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    private let groceriesCoreDataAccess: GroceriesCoreDataAccess = GroceriesCoreDataAccess()
    private var selectedRow: Int = 0
    
    @IBOutlet weak var boughtGroceriesLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let numberOfBoughtGroceries = groceriesCoreDataAccess.getNumOfBoughtGroceries()
        boughtGroceriesLabel.text = numberOfBoughtGroceries > 0 ? String(numberOfBoughtGroceries) : ""
        tabBarController?.tabBar.items![(tabBarController?.selectedIndex)!].badgeValue = numberOfBoughtGroceries > 0 ? String(numberOfBoughtGroceries) : nil
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "Raleway", size: 18)
        header.textLabel?.frame = header.frame
        header.textLabel?.textAlignment = NSTextAlignment.left
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 3) {
            switch (indexPath.row){
            case 0...3:
                selectedRow = indexPath.row
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "showAlertSegue", sender: self)
                }
            default:
                break
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAlertSegue" {
            let destination = segue.destination as! AlertViewController
            
            destination.sentRow = selectedRow
        }
    }

}
