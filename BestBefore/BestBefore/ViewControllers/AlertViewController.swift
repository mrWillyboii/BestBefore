//
//  AlertViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-07.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {
    
    private let coreDataAccess: CategoriesCoreDataAccess = CategoriesCoreDataAccess()

    @IBOutlet weak var popUpWindowView: UIView!
    @IBOutlet weak var infoText: UITextView!
    var sentRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        popUpWindowView.layer.shadowColor = UIColor.gray.cgColor
        popUpWindowView.layer.shadowOffset = CGSize(width: 0, height: 2)
        popUpWindowView.layer.shadowOpacity = 0.2
        popUpWindowView.layer.shadowRadius = 1.0
        popUpWindowView.layer.cornerRadius = 5

        // Do any additional setup after loading the view.
        if UIScreen.main.bounds.width <= 320.0 {
            infoText.font = UIFont(name: (infoText.font?.fontName)!, size: 11)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        
        if touch?.view != popUpWindowView {
            self.dismiss(animated: true, completion: nil);
        }
    }
    
    @IBAction func onCancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onYesPressed(_ sender: Any) {
        switch (sentRow){
        case 0:
            coreDataAccess.deleteCategoriesByStorage(storage: 0)
            break
        case 1:
            coreDataAccess.deleteCategoriesByStorage(storage: 1)
            break
        case 2:
            coreDataAccess.deleteCategoriesByStorage(storage: 2)
            break
        case 3:
            coreDataAccess.deleteAllCategories()
            break
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
}
