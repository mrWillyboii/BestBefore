//
//  AddMultipleGroceriesViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-05.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import CoreData

class AddMultipleGroceriesViewController: UIViewController {

    @IBOutlet weak var detailView: DetailView!
    private let coreDataAccess: CategoriesCoreDataAccess = CategoriesCoreDataAccess()
    private let groceriesCoreDataAccess : GroceriesCoreDataAccess = GroceriesCoreDataAccess()
    private var boughtGroceries: [NSManagedObject] = []
    private var currentGrocery: NSManagedObject = NSManagedObject()
    @IBOutlet weak var noGroceriesLeft: UILabel!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.extendedLayoutIncludesOpaqueBars = true
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isOpaque = true
        boughtGroceries = groceriesCoreDataAccess.getBoughtGroceries()
        tabBarController?.tabBar.items?.last?.badgeValue = boughtGroceries.count > 0 ? String(boughtGroceries.count) : nil
        
        setView()
    }
    
    public func setView(){
        if boughtGroceries.count > 0 {
            doneButton.isEnabled = true
            currentGrocery = boughtGroceries.first!
            let categoryId = currentGrocery.objectIDs(forRelationshipNamed: "category")[0]
            
            
            UIView.animate(withDuration: 0.3, animations: {
                self.detailView.alpha = 0.0
                self.detailView.fillView(self.currentGrocery, ((self.currentGrocery as! Grocery).category?.storage)!)
                self.detailView.chooseCategoryBy(id: categoryId)
                self.detailView.alpha = 1.0
            })
            
            detailView.datePickerView.isUserInteractionEnabled = true
            detailView.layer.shadowColor = UIColor.gray.cgColor
            detailView.layer.shadowOffset = CGSize(width: 0, height: 2)
            detailView.layer.shadowOpacity = 0.2
            detailView.layer.shadowRadius = 1.0
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.detailView.alpha = 0.0
            })
            self.noGroceriesLeft.isHidden = false
            self.detailView.removeFromSuperview()
            doneButton.isEnabled = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isOpaque = false
    }
    
    @IBAction func onDonePressed(_ sender: Any) {
        let grocery = currentGrocery as! Grocery
        let selectedCategory = detailView.getSelectedCategory()
        
        groceriesCoreDataAccess.editGrocery(name: grocery.name!, date: detailView.datePickerView.date as NSDate, image: UIImage(data: grocery.image! as Data)!, groceryId: currentGrocery.objectID, category: detailView.getPickerData()[selectedCategory])
        groceriesCoreDataAccess.deleteBoughtGrocery(currentGrocery.objectID)
        boughtGroceries = groceriesCoreDataAccess.getBoughtGroceries()
        tabBarController?.tabBar.items?.last?.badgeValue = boughtGroceries.count > 0 ? String(boughtGroceries.count) : nil
        setView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
