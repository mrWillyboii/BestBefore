//
//  GroceriesViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-11-13.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import CoreData

class GroceriesViewController: UIViewController {

    @IBOutlet weak var groceriesTableView: UITableView!
    var categoryId: NSManagedObjectID = NSManagedObjectID()
    var categoryName: String = ""
    var groceries: [NSManagedObject] = []
    let coreDataAccess: CategoriesCoreDataAccess = CategoriesCoreDataAccess()
    let groceriesCoreDataAccess : GroceriesCoreDataAccess = GroceriesCoreDataAccess()
    
    var selectedGrocery: NSManagedObject = NSManagedObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        groceriesTableView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        groceriesTableView.separatorStyle = .none
        
        self.title = categoryName.isEmpty ? "" : categoryName
    }
    
    override func viewWillAppear(_ animated: Bool) {
        groceries = groceriesCoreDataAccess.getAllGroceriesForCategory(categoryId: categoryId)
        
        self.groceriesTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension GroceriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groceries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("CustomTableViewCell", owner: self, options: nil)?.first as! CustomTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let currentGrocery = groceries[indexPath.row]
        
        cell.categoryLabel.text = currentGrocery.value(forKey: "name") as? String
        cell.categoryImageView.image = UIImage(data: currentGrocery.value(forKey: "image") as! Data)
        
        let numDaysLeft = groceriesCoreDataAccess.getGroceryNumOfDaysLeft(groceryId: currentGrocery.objectID)
        print(numDaysLeft)
        cell.oldGroceriesLabel.text = numDaysLeft <= 0 ? "!" : String(numDaysLeft)
        
        var labelColor: UIColor = UIColor()
        
        switch numDaysLeft {
        case ...1:
            labelColor = UIColor(red: 237/255, green: 115/255, blue: 111/255, alpha: 1)
        case 2...5:
            labelColor = UIColor(red: 237/255, green: 218/255, blue: 111/255, alpha: 1)
        case 6...10:
            labelColor = UIColor(red: 198/255, green: 237/255, blue: 111/255, alpha: 1)
        default:
            cell.oldGroceriesLabel.text = ">10"
            labelColor = UIColor(red: 105/255, green: 226/255, blue: 100/255, alpha: 1)
        }
        
        cell.oldGroceriesLabel.textColor = labelColor
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedGrocery = groceries[indexPath.row]
        
        print("GROCERY", selectedGrocery.value(forKey: "name") as! String)
        
        performSegue(withIdentifier: "showDetails", sender: self)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
        
            groceriesCoreDataAccess.deleteGrocery(groceryId: groceries[indexPath.row].objectID)
            groceries.remove(at: indexPath.row)
            tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            let destination = segue.destination as! ShowDetailViewController
            destination.grocery = selectedGrocery
            destination.categoryId = categoryId
        }
    }
}
