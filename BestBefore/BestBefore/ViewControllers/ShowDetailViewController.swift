//
//  ShowDetailViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-11-14.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import CoreData

class ShowDetailViewController: UIViewController {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var detailView: DetailView!
    
    let coreDataAccess : CategoriesCoreDataAccess = CategoriesCoreDataAccess()
    let groceriesCoreDataAccess : GroceriesCoreDataAccess = GroceriesCoreDataAccess()
    var grocery: NSManagedObject = NSManagedObject()
    var categoryId: NSManagedObjectID = NSManagedObjectID()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = editButtonItem
        
        initDetails(grocery)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.extendedLayoutIncludesOpaqueBars = true
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isOpaque = true
    }
    
    func initDetails(_ grocery: NSManagedObject){
        let tabBar = self.tabBarController as! UITabBarController
        
        detailView.fillView(grocery, (Int16(tabBar.selectedIndex)))
        detailView.chooseCategoryBy(id: categoryId)
        detailView.layer.shadowColor = UIColor.gray.cgColor
        detailView.layer.shadowOffset = CGSize(width: 0, height: 2)
        detailView.layer.shadowOpacity = 0.2
        detailView.layer.shadowRadius = 1.0
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        let barButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.onDoneButtonPressed(_:)))
        let addImageGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.addImage(_:)))
        detailView.groceryImageView.isUserInteractionEnabled = true
        detailView.groceryImageView.addGestureRecognizer(addImageGestureRecognizer)
        navigationItem.rightBarButtonItem = barButton
        detailView.makeEditable()
    }
    
    @objc func addImage(_ sender: UITapGestureRecognizer){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = storyboard.instantiateViewController(withIdentifier: "PopUpViewControllerId") as! ImagePickerPopUpViewController
        
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        popupVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(popupVC, animated: true, completion: nil)
    }
    
    @objc func onDoneButtonPressed(_ sender: Any) {
        if(checkIfTextfieldIsEmpty()) {
            return
        }
        
        let selectedCategory = detailView.getSelectedCategory()
        
        if(selectedCategory == detailView.getPickerData().count) {
            categoryId = coreDataAccess.addCategory(name: "", storage: detailView.getSelectedStorage(), image: UIImage())
            
            
            let cat = coreDataAccess.getCategoryById(categoryId: categoryId)
            
            groceriesCoreDataAccess.editGrocery(name: detailView.nameTextField.text!, date: detailView.datePickerView.date as NSDate, image: detailView.groceryImageView.image!, groceryId: grocery.objectID, category: cat)
            
        } else {
            groceriesCoreDataAccess.editGrocery(name: detailView.nameTextField.text!, date: detailView.datePickerView.date as NSDate, image: detailView.groceryImageView.image!, groceryId: grocery.objectID, category: detailView.getPickerData()[selectedCategory])
        }
        
        groceriesCoreDataAccess.deleteBoughtGrocery(grocery.objectID)
        
        if(detailView.getSelectedCategory() == detailView.getPickerData().count) {
            performSegue(withIdentifier: "addCategorySegue", sender: self)
        }else{
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    func setImageView(image: UIImage){
        detailView.groceryImageView.image = image
    }
    
    func checkIfTextfieldIsEmpty() -> Bool {
        if(detailView.nameTextField.text?.isEmpty)! {
            detailView.nameTextField.layer.borderWidth = 1.0
            detailView.nameTextField.layer.borderColor = UIColor.red.cgColor
            return true
        }
        else {
            detailView.nameTextField.layer.borderWidth = 0.0
            detailView.nameTextField.layer.borderColor = UIColor.black.cgColor
            return false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "addCategorySegue") {
            let destination = segue.destination as! AddCategoryViewController
            destination.categoryId = categoryId
            destination.flag = "done"
        }
    }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isOpaque = false
    }

}
