//
//  GeneratedListViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-03.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import CoreData

class GeneratedListViewController: UIViewController {
    
    private var groceriesByStorage : [[NSManagedObject]] = [[], [], []]
    private var boughtGroceries: [NSManagedObject] = []
    private let userSettingsCoreDataAccess : UserSettingsCoreDataAccess = UserSettingsCoreDataAccess()
    
    private let groceriesCoreDataAccess : GroceriesCoreDataAccess = GroceriesCoreDataAccess()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        tableView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        tableView.separatorStyle = .none
        
        let wantedDays = userSettingsCoreDataAccess.getNumberOfDaysLeft()
        let excludedStorages = userSettingsCoreDataAccess.getExcludedStorages()
        let groceryList = groceriesCoreDataAccess.getWantedExpiringGroceries(daysUntilExpireDate: wantedDays)
        
        tableView?.allowsMultipleSelection = true
        
        if groceryList.isEmpty{
            doneButton.isEnabled = false
        }
        
        for grocery in groceryList {
            let g  = grocery as! Grocery
            switch g.category?.storage {
            case Int16(0)?:
                if !excludedStorages[0]{
                    groceriesByStorage[0].append(g)
                }
                break;
            case Int16(1)?:
                if !excludedStorages[1]{
                    groceriesByStorage[1].append(g)
                }
                break;
            case Int16(2)?:
                if !excludedStorages[2]{
                    groceriesByStorage[2].append(g)
                }
                break;
            default:
                break;
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func createBoughtGroceriesList(){
        for indexPath in tableView.indexPathsForSelectedRows! {
            boughtGroceries.append(groceriesByStorage[indexPath.section][indexPath.row])
        }
        
        groceriesCoreDataAccess.addBoughtGroceries(boughtGroceries)
        navigationController?.popToRootViewController(animated: true)
    }
    
}

extension GeneratedListViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        var numberOfSectionsNeeded : Int = 0
        for groceriesInStorage in groceriesByStorage {
            if(!groceriesInStorage.isEmpty) {
                numberOfSectionsNeeded += 1
            }
        }
        print(numberOfSectionsNeeded)
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groceriesByStorage[section].count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let storagePantryString = NSLocalizedString("pantry", comment: "")
        let storageFreezerString = NSLocalizedString("freezer", comment: "")
        let storageFridgeString = NSLocalizedString("fridge", comment: "")
        
        var currentHeader: String = ""
        
        switch section {
        case 0:
            currentHeader = storageFridgeString
        case 1:
            currentHeader = storageFreezerString
        case 2:
            currentHeader = storagePantryString
        default:
            break
        }
        
        return currentHeader
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if groceriesByStorage[section].isEmpty {
            return NSLocalizedString("there_are_no_old", comment: "")
        }
        
        return ""
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("CustomShoppingListItemTableViewCell", owner: self, options: nil)?.first as! CustomShoppingListItemTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.groceryLabel.text = (groceriesByStorage[indexPath.section][indexPath.row] as! Grocery).name
        cell.groceryImageView.image = UIImage(data: (groceriesByStorage[indexPath.section][indexPath.row] as! Grocery).image! as Data)

        return cell
    }
}
