//
//  SettingsPopUpViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-11-13.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class SettingsPopUpViewController: UIViewController {

    @IBOutlet weak var popUpWindowView: UIView!
    @IBOutlet weak var imageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var popUpImageView: UIImageView!
    @IBOutlet weak var aboutTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        popUpWindowView.layer.shadowColor = UIColor.gray.cgColor
        popUpWindowView.layer.shadowOffset = CGSize(width: 0, height: 2)
        popUpWindowView.layer.shadowOpacity = 0.2
        popUpWindowView.layer.shadowRadius = 1.0
        popUpWindowView.layer.cornerRadius = 5

        // Do any additional setup after loading the view.
        let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let versionString = NSLocalizedString("version", comment: "")
        let createdByString = NSLocalizedString("created_by", comment: "")
        
        aboutTextView.text = versionString+" "+version+"\n\n"+createdByString+"\nKonstantin Ay\nWilly Larsson"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        
        if touch?.view != popUpWindowView {
            self.dismiss(animated: true, completion: nil);
        }
    }

}
