//
//  ShoppingListSettingsTableViewController.swift
//  BestBefore
//
//  Created by Willy Larsson on 2017-12-05.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class ShoppingListSettingsTableViewController: UITableViewController {
    
    var excludedStorages : [Bool] = [false, false, false]
    var numberOfDaysLeft : Int16 = 0
    
    @IBOutlet weak var amountOfDays: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var fridgeSwitch: UISwitch!
    @IBOutlet weak var freezerSwitch: UISwitch!
    @IBOutlet weak var pantrySwitch: UISwitch!
    @IBOutlet weak var middleTextLabel: UILabel!
    
    let userSettingsCoreDataAccess : UserSettingsCoreDataAccess = UserSettingsCoreDataAccess()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberOfDaysLeft = userSettingsCoreDataAccess.getNumberOfDaysLeft()
        stepper.value = Double(numberOfDaysLeft)
        amountOfDays.text = String(numberOfDaysLeft)
        
        excludedStorages = userSettingsCoreDataAccess.getExcludedStorages()
        fridgeSwitch.isOn = excludedStorages[0]
        freezerSwitch.isOn = excludedStorages[1]
        pantrySwitch.isOn = excludedStorages[2]
        
        if UIScreen.main.bounds.width <= 320.0 {
            middleTextLabel.font = UIFont(name: (middleTextLabel.font?.fontName)!, size: 14)
        }
    }

    @IBAction func stepper(_ sender: UIStepper) {
        numberOfDaysLeft = Int16(sender.value)
        amountOfDays.text = String(numberOfDaysLeft)
    }
    
    @IBAction func excludeFridge(_ sender: UISwitch) {
        excludedStorages[0] = sender.isOn
    }
    
    @IBAction func excludeFreezer(_ sender: UISwitch) {
        excludedStorages[1] = sender.isOn
    }
    
    @IBAction func excludePantry(_ sender: UISwitch) {
        excludedStorages[2] = sender.isOn
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        userSettingsCoreDataAccess.setExcludedStorages(excludedStorages: excludedStorages)
        userSettingsCoreDataAccess.setNumberOfDaysLeft(numberOfDaysLeft: numberOfDaysLeft)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
