//
//  OurLibraryViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-10.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class OurLibraryViewController: UIViewController, UINavigationBarDelegate {
    private let reuseIdentifier = "photoCell"
    private var UIImageArray: [UIImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        fillArray()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func fillArray(){
        let imageArray = Bundle.main.urls(forResourcesWithExtension: "png", subdirectory: "OurLibrary")! as [NSURL]
        
        print(imageArray)
        
        do {
            for url in imageArray {
                let data = try NSData(contentsOf: url as URL) as Data
                UIImageArray.append(UIImage(data: data)!)
            }
        } catch {
            print(error)
        }
    }

}

extension OurLibraryViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return UIImageArray.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ImageCollectionViewCell
        
        cell.photoImageView.image = UIImageArray[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionView, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let screensize: CGRect = UIScreen.main.bounds
        var cellWidth = 0
        var cellHeight = 0
        
        if screensize.width <= 320 {
            cellWidth = 120
            cellHeight = 120
        } else {
            cellWidth = 150
            cellHeight = 150
        }
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ImageCollectionViewCell
        
        let presentingVC = presentingViewController as! ImagePickerPopUpViewController
        presentingVC.pickedImage = cell.photoImageView.image!
        self.dismiss(animated: true, completion: nil)
        presentingVC.returnImage()
    }
}
