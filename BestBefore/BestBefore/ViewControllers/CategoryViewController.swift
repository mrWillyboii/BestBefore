//
//  CategoryViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-11-06.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import CoreData

class CategoryViewController: UIViewController {

    @IBOutlet weak var groceryCategoryTableView: UITableView!
    
    var categories: [NSManagedObject] = []
    let categoriesCoreDataAccess: CategoriesCoreDataAccess = CategoriesCoreDataAccess()
    let groceriesCoreDataAccess : GroceriesCoreDataAccess = GroceriesCoreDataAccess()
    var categoryId: NSManagedObjectID = NSManagedObjectID()
    var chosenCategory: NSManagedObject = NSManagedObject()
    var categoryName: String = ""
    var selectedAddCategory: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let boughtGroceries = groceriesCoreDataAccess.getBoughtGroceries()
        tabBarController?.tabBar.items?.last?.badgeValue = boughtGroceries.count > 0 ? String(boughtGroceries.count) : nil
        
        groceryCategoryTableView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        groceryCategoryTableView.separatorStyle = .none
        setTitle()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        categories = categoriesCoreDataAccess.getCategoriesByStorage(storage: Int16((self.tabBarController?.selectedIndex)!))
        self.groceryCategoryTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTitle(){
        let selectedTab: Int = (self.tabBarController?.selectedIndex)!
        let storagePantryString = NSLocalizedString("pantry", comment: "")
        let storageFreezerString = NSLocalizedString("freezer", comment: "")
        let storageFridgeString = NSLocalizedString("fridge", comment: "")
        
        switch selectedTab{
        case 0:
            self.navigationItem.title = storageFridgeString
        case 1:
            self.navigationItem.title = storageFreezerString
        case 2:
            self.navigationItem.title = storagePantryString
        default:
            return
        }
    }
}

extension CategoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if categories.isEmpty || indexPath.row == categories.count {
            let cell = Bundle.main.loadNibNamed("AddCategoryTableViewCell", owner: self, options: nil)?.first as! AddCategoryTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell
        } else {
            let cell = Bundle.main.loadNibNamed("CustomTableViewCell", owner: self, options: nil)?.first as! CustomTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            print(categories)
            let currentCategory = categories[indexPath.row]
            
            cell.categoryLabel.text = currentCategory.value(forKey: "name") as? String
            
            cell.categoryImageView.image = UIImage(data: currentCategory.value(forKey: "image") as! Data)
            
            let numOfOldGroceries = groceriesCoreDataAccess.getNumOfOldGroceriesForCategory(categoryId: currentCategory.objectID)
            cell.oldGroceriesLabel.text = numOfOldGroceries == 0 ? "" : String(numOfOldGroceries)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if categories.isEmpty || indexPath.row == categories.count {
            selectedAddCategory = true
            performSegue(withIdentifier: "addCategory", sender: self)
        } else {
            categoryId = categories[indexPath.row].objectID
            categoryName = (categories[indexPath.row] as! Category).name!
            
            performSegue(withIdentifier: "showGroceries", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if categories.isEmpty || indexPath.row == categories.count {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if categories.count > 0 && indexPath.row < categories.count{
            let deleteString = NSLocalizedString("delete", comment: "")
            let editString = NSLocalizedString("edit", comment: "")
            
            let delete = UITableViewRowAction(style: .default, title: deleteString) { (action:UITableViewRowAction, indexPath:IndexPath) in
                self.categoriesCoreDataAccess.deleteCategory(categoryId: self.categories[indexPath.row].objectID)
                self.categories.remove(at: indexPath.row)
                tableView.reloadData()
            }
            delete.backgroundColor = .red
            
            let edit = UITableViewRowAction(style: .default, title: editString) { (action:UITableViewRowAction, indexPath:IndexPath) in
                self.chosenCategory = self.categories[indexPath.row]
                self.performSegue(withIdentifier: "showCategory", sender: self)
            }
            edit.backgroundColor = .orange
            return [edit, delete]
        }
        return []
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showGroceries" {
            let destination = segue.destination as! GroceriesViewController
            destination.categoryId = categoryId
            destination.categoryName = categoryName
        } else if segue.identifier == "showCategory"{
            let destination = segue.destination as! ShowCategoryDetailViewController
            destination.category = chosenCategory
            
        }
    }
    
}
