//
 //  SplashViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-11-10.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var arrow: UIImageView!
    @IBOutlet weak var bestBefore: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        bestBefore.transform = CGAffineTransform(scaleX: 0.01, y: 0.01);
        
        UIView.animate(withDuration: 1.0, animations: {
            self.bestBefore.transform = CGAffineTransform(scaleX: 1.0, y: 1.0);
            self.arrow.transform = self.arrow.transform.rotated(by: CGFloat(Double.pi))
            self.arrow.transform = self.arrow.transform.rotated(by: CGFloat(Double.pi))
        }, completion: { finished in
            self.performSegue(withIdentifier: "splashSegue", sender: self)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
