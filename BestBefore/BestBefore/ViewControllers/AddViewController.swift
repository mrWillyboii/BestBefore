//
//  AddViewController.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-11-07.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import DLRadioButton
import CoreData

class AddViewController: UIViewController{

    @IBOutlet weak var detailView: DetailView!
    let coreDataAccess : CategoriesCoreDataAccess = CategoriesCoreDataAccess()
    let groceriesCoreDataAccess : GroceriesCoreDataAccess = GroceriesCoreDataAccess()
    var storage : Int16 = 0
    var selectedCategory: Int = 0
    var categoryId: NSManagedObjectID = NSManagedObjectID()
    var flag : String = String()
    
    var pickerData: [NSManagedObject] = [NSManagedObject]()
    var imagePickerView: UIPickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        detailView.layer.shadowColor = UIColor.gray.cgColor
        detailView.layer.shadowOffset = CGSize(width: 0, height: 2)
        detailView.layer.shadowOpacity = 0.2
        detailView.layer.shadowRadius = 1.0

        setStorageRadioButton()
        detailView.addNewStackView.isHidden = false
        
        let newItemGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.addNewClicked(_:)))
        detailView.addNewStackView.isUserInteractionEnabled = true
        detailView.addNewStackView.addGestureRecognizer(newItemGestureRecognizer)
        
        let addImageGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.addImage(_:)))
        detailView.groceryImageView.isUserInteractionEnabled = true
        detailView.groceryImageView.addGestureRecognizer(addImageGestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.extendedLayoutIncludesOpaqueBars = true
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isOpaque = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isOpaque = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        detailView.reloadCategories()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setStorageRadioButton(){
        switch tabBarController?.selectedIndex {
        case 0?:
            detailView.fridgeRadioButton.sendActions(for: .touchUpInside)
        case 1?:
            detailView.freezerRadioButton.sendActions(for: .touchUpInside)
        case 2?:
            detailView.pantryRadioButton.sendActions(for: .touchUpInside)
        default:
            break
        }
    }
    
    @objc func addNewClicked(_ sender: UITapGestureRecognizer){
        if(checkIfTextfieldIsEmpty()) {
            return
        }

        if(detailView.getSelectedCategory() == detailView.getPickerData().count) {
            categoryId = coreDataAccess.addCategory(name: "", storage: detailView.getSelectedStorage(), image: UIImage())
            groceriesCoreDataAccess.addGrocery(name: detailView.nameTextField.text!, date: detailView.datePickerView.date as NSDate, image: detailView.groceryImageView.image!, categoryId: categoryId)
        } else {
            groceriesCoreDataAccess.addGrocery(name: detailView.nameTextField.text!, date: detailView.datePickerView.date as NSDate, image: detailView.groceryImageView.image!, categoryId: detailView.getPickerData()[detailView.getSelectedCategory()].objectID)
        }

        detailView.nameTextField.text = ""
        detailView.categoryPickerView.selectRow(0, inComponent: 0, animated: true)
        detailView.groceryImageView.image =  #imageLiteral(resourceName: "apple-1")
        detailView.datePickerView.date = Date()

        if(detailView.getSelectedCategory() == detailView.getPickerData().count) {
            flag = "add more"
            performSegue(withIdentifier: "addCategorySegue", sender: self)
        }

    }
    
    @objc func addImage(_ sender: UITapGestureRecognizer){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = storyboard.instantiateViewController(withIdentifier: "PopUpViewControllerId") as! ImagePickerPopUpViewController

        popupVC.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        popupVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(popupVC, animated: true, completion: nil)
    }
    
    
    @IBAction func doneButtonClicked(_ sender: Any) {
        if(checkIfTextfieldIsEmpty()) {
            return
        }

        if(detailView.getSelectedCategory() == detailView.getPickerData().count) {
            categoryId = coreDataAccess.addCategory(name: "", storage: detailView.getSelectedStorage(), image: UIImage())
            groceriesCoreDataAccess.addGrocery(name: detailView.nameTextField.text!, date: detailView.datePickerView.date as NSDate, image: detailView.groceryImageView.image!, categoryId: categoryId)
            flag = "done"
            performSegue(withIdentifier: "addCategorySegue", sender: self)
        } else {
            groceriesCoreDataAccess.addGrocery(name: detailView.nameTextField.text!, date: detailView.datePickerView.date as NSDate, image: detailView.groceryImageView.image!, categoryId: detailView.getPickerData()[detailView.getSelectedCategory()].objectID)

            _ = navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func checkIfTextfieldIsEmpty() -> Bool {
        if(detailView.nameTextField.text?.isEmpty)! {
            detailView.nameTextField.layer.borderWidth = 1.0
            detailView.nameTextField.layer.borderColor = UIColor.red.cgColor
            return true
        }
        else {
            detailView.nameTextField.layer.borderWidth = 0.0
            detailView.nameTextField.layer.borderColor = UIColor.black.cgColor
            return false
        }
    }
    
    func setImageView(image: UIImage){
        detailView.groceryImageView.image = image
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "addCategorySegue") {
            let destination = segue.destination as! AddCategoryViewController
            destination.categoryId = categoryId
            destination.flag = flag
            destination.cameFromAddGrocery = true
        }
    }
}

