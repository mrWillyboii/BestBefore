//
//  ShoppingListTableViewCell.swift
//  BestBefore
//
//  Created by Willy Larsson on 2017-11-22.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class ShoppingListTableViewCell: UITableViewCell {

    @IBOutlet weak var groceryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
