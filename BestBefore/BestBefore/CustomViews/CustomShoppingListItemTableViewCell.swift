//
//  ShoppingListItemTableViewCell.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-04.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class CustomShoppingListItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var groceryImageView: UIImageView!
    @IBOutlet weak var groceryLabel: UILabel!
    @IBOutlet weak var innerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        innerView.layer.shadowColor = UIColor.gray.cgColor
        innerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        innerView.layer.shadowOpacity = 0.2
        innerView.layer.shadowRadius = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)
        if selected {
            innerView.layer.shadowOpacity = 0.0
            innerView.alpha = 0.4
        } else {
            innerView.layer.shadowOpacity = 0.2
            innerView.alpha = 1.0
        }
    }
}
