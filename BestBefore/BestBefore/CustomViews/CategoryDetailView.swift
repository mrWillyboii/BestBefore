//
//  CategoryDetailView.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-11.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import DLRadioButton
import CoreData

class CategoryDetailView: UIView {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var radioButtonStackView: UIStackView!
    @IBOutlet weak var fridgeRadioButton: DLRadioButton!
    @IBOutlet weak var freezerRadioButton: DLRadioButton!
    @IBOutlet weak var pantryRadioButton: DLRadioButton!
    @IBOutlet var contentView: CategoryDetailView!
    
    private var coreDataAccess: CategoriesCoreDataAccess = CategoriesCoreDataAccess()
    
    private var storage: Int16 = 0
    private var selectedCategory: Int = 0
    
    var viewType: Int = 1
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func dismissKeyboard(_ sender: Any) {
        self.resignFirstResponder()
    }
    
    func initView() {
        Bundle.main.loadNibNamed("CategoryDetailView", owner: self, options: nil)
        
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func fillView(_ category: NSManagedObject, _ storage: Int16){
        categoryImageView.image = UIImage(data: category.value(forKey: "image") as! Data)
        nameTextField.text = (category.value(forKey: "name") as! String)
        
        setStorageRadioButton(storage)
        
        nameTextField.isEnabled = false
        radioButtonStackView.isUserInteractionEnabled = false
        categoryImageView.isUserInteractionEnabled = false
    }
    
    func makeEditable() {
        nameTextField.isEnabled = true
        radioButtonStackView.isUserInteractionEnabled = true
        categoryImageView.isUserInteractionEnabled = true
    }
    
    func setStorageRadioButton(_ storage: Int16){
        switch storage {
        case 0:
            fridgeRadioButton.sendActions(for: .touchUpInside)
        case 1:
            freezerRadioButton.sendActions(for: .touchUpInside)
        case 2:
            pantryRadioButton.sendActions(for: .touchUpInside)
        default:
            break
        }
    }

    @IBAction func buttonSelected(_ radioButton: DLRadioButton) {
        switch radioButton {
        case fridgeRadioButton:
            storage = 0
            break
        case freezerRadioButton:
            storage = 1
            break
        case pantryRadioButton:
            storage = 2
            break
        default:
            break;
        }
    }
    
    public func getSelectedStorage() -> Int16 {
        return storage
    }
}
