//
//  CategoryTableViewCell.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-11-07.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var oldGroceriesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        innerView.layer.shadowColor = UIColor.gray.cgColor
        innerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        innerView.layer.shadowOpacity = 0.2
        innerView.layer.shadowRadius = 1.0
    }
    
}
