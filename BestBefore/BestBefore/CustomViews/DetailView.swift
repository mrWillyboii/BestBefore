//
//  DetailView.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-11-14.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import DLRadioButton
import CoreData

class DetailView: UIView {

    @IBOutlet weak var groceryImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var categoryPickerView: UIPickerView!
    @IBOutlet weak var datePickerView: UIDatePicker!
    @IBOutlet weak var addNewStackView: UIStackView!
    @IBOutlet weak var radioButtonStackView: UIStackView!
    
    @IBOutlet weak var fridgeRadioButton: DLRadioButton!
    @IBOutlet weak var freezerRadioButton: DLRadioButton!
    @IBOutlet weak var pantryRadioButton: DLRadioButton!
    
    @IBOutlet var contentView: UIView!
    
    private var pickerData: [NSManagedObject] = []
    private var coreDataAccess: CategoriesCoreDataAccess = CategoriesCoreDataAccess()
    
    private var storage: Int16 = 0
    private var selectedCategory: Int = 0
    
    var viewType: Int = 1
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        if viewType == 0 {
        }else{
            initView()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if viewType == 0 {
        }else{
            initView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func initView() {
        Bundle.main.loadNibNamed("DetailView", owner: self, options: nil)
        addNewStackView.isHidden = true
            
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func selectRadioButton(_ storage: Int16) {
        switch storage {
        case 0:
            fridgeRadioButton.sendActions(for: .touchUpInside)
            break
        case 1:
            freezerRadioButton.sendActions(for: .touchUpInside)
            break
        case 2:
            pantryRadioButton.sendActions(for: .touchUpInside)
            break
        default:
            return
        }
    }
    
    func chooseCategoryBy(id: NSManagedObjectID){
        let category = coreDataAccess.getCategoryById(categoryId: id)
        
        categoryPickerView.selectRow(pickerData.index(of: category)!, inComponent: 0, animated: true)
        self.pickerView(categoryPickerView, didSelectRow: pickerData.index(of: category)!, inComponent: 0)
    }
    
    func fillView(_ grocery: NSManagedObject, _ storage: Int16){
        groceryImageView.image = UIImage(data: grocery.value(forKey: "image") as! Data)
        nameTextField.text = (grocery.value(forKey: "name") as! String)
        
        selectRadioButton(storage)

        pickerData = coreDataAccess.getCategoriesByStorage(storage: Int16(storage))
        
        datePickerView.setDate(grocery.value(forKey: "date") as! Date, animated: true)
        categoryPickerView.reloadAllComponents()
        
        nameTextField.isEnabled = false
        categoryPickerView.isUserInteractionEnabled = false
        datePickerView.isUserInteractionEnabled = false
        radioButtonStackView.isUserInteractionEnabled = false
        groceryImageView.isUserInteractionEnabled = false
    }

    @IBAction func buttonSelected(_ radioButton: DLRadioButton) {
        switch radioButton {
        case fridgeRadioButton:
            storage = 0
            break
        case freezerRadioButton:
            storage = 1
            break
        case pantryRadioButton:
            storage = 2
            break
        default:
            break;
        }
        pickerData = coreDataAccess.getCategoriesByStorage(storage: storage)
        categoryPickerView.reloadAllComponents()
        selectedCategory = 0
        categoryPickerView.selectRow(0, inComponent: 0, animated: true)
    }
    
    func makeEditable() {
        nameTextField.isEnabled = true
        categoryPickerView.isUserInteractionEnabled = true
        datePickerView.isUserInteractionEnabled = true
        radioButtonStackView.isUserInteractionEnabled = true
        groceryImageView.isUserInteractionEnabled = true
    }
    
    @IBAction func dismissKeyboard(_ sender: Any) {
        self.resignFirstResponder()
    }
    
    func getPickerData() -> [NSManagedObject]{
        return pickerData;
    }
    
    func getSelectedStorage() -> Int16{
        return storage
    }
    
    func getSelectedCategory() -> Int{
        return selectedCategory
    }
    
    func reloadCategories(){
        pickerData = coreDataAccess.getCategoriesByStorage(storage: storage)
        categoryPickerView.reloadAllComponents()
    }
}

extension DetailView: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCategory = row
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        let createNewCategoryString = NSLocalizedString("create_new_category", comment: "")
        
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Raleway", size: 20)
            pickerLabel?.textAlignment = .center
        }
        
        if row == pickerData.count {
            pickerLabel?.textColor = UIColor(red: 237/255, green: 218/255, blue: 111/255, alpha: 1)
            pickerLabel?.text = createNewCategoryString
        } else {
            pickerLabel?.text = pickerData[row].value(forKey: "name") as? String
        }
        
        return pickerLabel!
    }
}

