//
//  AddCategoryTableViewCell.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-09.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class AddCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var cellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        innerView.layer.shadowColor = UIColor.gray.cgColor
        innerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        innerView.layer.shadowOpacity = 0.2
        innerView.layer.shadowRadius = 1.0

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
