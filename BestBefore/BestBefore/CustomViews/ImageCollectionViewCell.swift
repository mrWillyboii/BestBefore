//
//  ImageCollectionViewCell.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-10.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    
}
