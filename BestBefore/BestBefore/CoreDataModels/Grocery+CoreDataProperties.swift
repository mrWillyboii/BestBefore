//
//  Grocery+CoreDataProperties.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-05.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//
//

import Foundation
import CoreData


extension Grocery {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Grocery> {
        return NSFetchRequest<Grocery>(entityName: "Grocery")
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var image: NSData?
    @NSManaged public var name: String?
    @NSManaged public var category: Category?
    @NSManaged public var bought: BoughtGroceries?

}
