//
//  Category+CoreDataProperties.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-09.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//
//

import Foundation
import CoreData


extension Category {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Category> {
        return NSFetchRequest<Category>(entityName: "Category")
    }

    @NSManaged public var image: NSData?
    @NSManaged public var name: String?
    @NSManaged public var storage: Int16
    @NSManaged public var groceries: NSSet?

}

// MARK: Generated accessors for groceries
extension Category {

    @objc(addGroceriesObject:)
    @NSManaged public func addToGroceries(_ value: Grocery)

    @objc(removeGroceriesObject:)
    @NSManaged public func removeFromGroceries(_ value: Grocery)

    @objc(addGroceries:)
    @NSManaged public func addToGroceries(_ values: NSSet)

    @objc(removeGroceries:)
    @NSManaged public func removeFromGroceries(_ values: NSSet)

}
