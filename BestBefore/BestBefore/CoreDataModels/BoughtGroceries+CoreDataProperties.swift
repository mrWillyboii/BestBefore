//
//  BoughtGroceries+CoreDataProperties.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-05.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//
//

import Foundation
import CoreData


extension BoughtGroceries {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BoughtGroceries> {
        return NSFetchRequest<BoughtGroceries>(entityName: "BoughtGroceries")
    }

    @NSManaged public var groceries: NSSet?

}

// MARK: Generated accessors for groceries
extension BoughtGroceries {

    @objc(addGroceriesObject:)
    @NSManaged public func addToGroceries(_ value: Grocery)

    @objc(removeGroceriesObject:)
    @NSManaged public func removeFromGroceries(_ value: Grocery)

    @objc(addGroceries:)
    @NSManaged public func addToGroceries(_ values: NSSet)

    @objc(removeGroceries:)
    @NSManaged public func removeFromGroceries(_ values: NSSet)

}
