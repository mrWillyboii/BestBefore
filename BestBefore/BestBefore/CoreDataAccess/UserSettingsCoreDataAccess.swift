//
//  UserSettingsCoreDataAccess.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-11.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class UserSettingsCoreDataAccess {
    
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    public func setNumberOfDaysLeft(numberOfDaysLeft : Int16) {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User_settings")
            
            var result = try managedContext.fetch(fetchRequest)
            
            if(result.count == 0) {
                let entity = NSEntityDescription.entity(forEntityName: "User_settings", in: managedContext)!
                
                let userSettings = NSManagedObject(entity: entity, insertInto: managedContext)
                
                userSettings.setValue(numberOfDaysLeft, forKey: "numberOfDaysLeft")
            } else {
                result[0].setValue(numberOfDaysLeft, forKeyPath: "numberOfDaysLeft")
            }
            
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    public func getNumberOfDaysLeft() -> Int16 {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User_settings")
            
            let result = try managedContext.fetch(fetchRequest)
            
            if (result.count > 0)
            {
                return result[0].value(forKey: "numberOfDaysLeft") as! Int16
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return 0
    }
    
    public func setExcludedStorages(excludedStorages: [Bool]) {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User_settings")
            
            var result = try managedContext.fetch(fetchRequest)
            
            if(result.count == 0) {
                let entity = NSEntityDescription.entity(forEntityName: "User_settings", in: managedContext)!
                
                let userSettings = NSManagedObject(entity: entity, insertInto: managedContext)
                
                userSettings.setValue(excludedStorages, forKey: "excludedStorages")
            } else {
                result[0].setValue(excludedStorages, forKeyPath: "excludedStorages")
            }
            
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    public func getExcludedStorages() -> [Bool] {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User_settings")
            
            let result = try managedContext.fetch(fetchRequest)
            
            if (result.count > 0)
            {
                return result[0].value(forKey: "excludedStorages") as! [Bool]
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return [false, false, false]
    }
}
