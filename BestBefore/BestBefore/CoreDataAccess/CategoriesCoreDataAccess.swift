//
//  CoreDataAccess.swift
//  BestBefore
//
//  Created by Willy Larsson on 2017-11-13.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class CategoriesCoreDataAccess {
    
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    public func getAllCategories() -> [NSManagedObject] {
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Category")
        
        let groceryCategory = NSSortDescriptor(key: "name", ascending: true);
        
        fetchRequest.sortDescriptors = [groceryCategory];
        
        do {
            return try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return []
    }
    
    public func getCategoriesByStorage(storage: Int16) -> [NSManagedObject]{
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Category")
        fetchRequest.predicate = NSPredicate(format: "storage == %d", storage)
        
        let groceryCategory = NSSortDescriptor(key: "name", ascending: true);
        
        fetchRequest.sortDescriptors = [groceryCategory];
        
        do {
            return try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return []
    }
    
    public func deleteCategoriesByStorage(storage: Int16) {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Category")
        fetchRequest.predicate = NSPredicate(format: "storage == %d", storage)
        
        do {
            for category in try managedContext.fetch(fetchRequest) {
                managedContext.delete(category)
            }
            try managedContext.save()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    public func getCategoryById(categoryId: NSManagedObjectID) -> NSManagedObject{
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            let fetchRequest = NSFetchRequest<Category>(entityName: "Category")
            
            fetchRequest.predicate = NSPredicate(format: "self == %@", categoryId)
            
            let result = try managedContext.fetch(fetchRequest)
            
            if (result.count > 0)
            {
                return result[0]
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return NSManagedObject()
    }
    
    public func addCategory(name : String, storage: Int16, image : UIImage) -> NSManagedObjectID {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Category", in: managedContext)!
        
        let category = NSManagedObject(entity: entity, insertInto: managedContext)
        
        let convertedImage = UIImageJPEGRepresentation(image, 1)
        
        category.setValue(name, forKeyPath: "name")
        category.setValue(storage, forKey: "storage")
        category.setValue(convertedImage, forKey: "image")
        
        
        do {
            try managedContext.save()
            return category.objectID
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        return NSManagedObjectID()
    }
    
    public func deleteCategory(categoryId : NSManagedObjectID) {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            let fetchRequest = NSFetchRequest<Category>(entityName: "Category")
            
            fetchRequest.predicate = NSPredicate(format: "self == %@", categoryId)
            
            let result = try managedContext.fetch(fetchRequest)
            
            if (result.count > 0)
            {
                let category = result[0]
                for grocery in category.groceries?.sortedArray(using: [NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSNumber.compare(_:)))]) as! [Grocery] {
                    managedContext.delete(grocery)
                }
                managedContext.delete(category)
                try managedContext.save()
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

    }
    
    public func editCategory(name : String, image : UIImage, categoryId : NSManagedObjectID, storage: Int16) {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            let fetchRequest = NSFetchRequest<Category>(entityName: "Category")
            
            fetchRequest.predicate = NSPredicate(format: "self == %@", categoryId)
            
            let result = try managedContext.fetch(fetchRequest)
            
            if (result.count > 0)
            {
                let category = result[0]
                category.setValue(name, forKey: "name")
                category.setValue(storage, forKey: "storage")
                let convertedImage = UIImageJPEGRepresentation(image, 1)
                category.setValue(convertedImage, forKey: "image")
                try managedContext.save()
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    public func deleteAllCategories() {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Category")
        
        do {
            for category in try managedContext.fetch(fetchRequest) {
                print(category)
                managedContext.delete(category)
            }
            try managedContext.save()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}
