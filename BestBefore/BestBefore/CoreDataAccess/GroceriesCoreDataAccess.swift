//
//  GroceriesCoreDataAccess.swift
//  BestBefore
//
//  Created by Konstantin Ay on 2017-12-11.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class GroceriesCoreDataAccess {
    
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    public func getAllGroceriesForCategory(categoryId : NSManagedObjectID) -> [NSManagedObject] {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            let fetchRequest = NSFetchRequest<Category>(entityName: "Category")
            
            fetchRequest.predicate = NSPredicate(format: "self == %@", categoryId)
            
            let result = try managedContext.fetch(fetchRequest)
            
            if (result.count > 0)
            {
                let category = result[0]
                return category.groceries?.sortedArray(using: [NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSNumber.compare(_:)))]) as! [Grocery]
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return []
    }
    
    public func getNumOfOldGroceriesForCategory(categoryId : NSManagedObjectID) -> Int {
        let managedContext = appDelegate.persistentContainer.viewContext
        let today = NSDate()
        var numberOfOldGroceries: Int = 0
        
        do {
            let fetchRequest = NSFetchRequest<Category>(entityName: "Category")
            
            fetchRequest.predicate = NSPredicate(format: "self == %@", categoryId)
            
            let result = try managedContext.fetch(fetchRequest)
            
            if (result.count > 0){
                let category = result[0]
                print(category)
                for grocery in category.groceries! {
                    if (grocery as! Grocery).date?.compare(today as Date) == ComparisonResult.orderedAscending {
                        numberOfOldGroceries += 1
                    }
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return numberOfOldGroceries
    }
    
    public func addGrocery(name : String, date : NSDate, image : UIImage, categoryId : NSManagedObjectID) {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let convertedImage = UIImageJPEGRepresentation(image, 1)
        
        let grocery = Grocery.init(entity: NSEntityDescription.entity(forEntityName: "Grocery", in:managedContext)!, insertInto: managedContext)
        grocery.name = name
        grocery.date = date
        grocery.image = convertedImage! as NSData
        
        do {
            let fetchRequest = NSFetchRequest<Category>(entityName: "Category")
            
            fetchRequest.predicate = NSPredicate(format: "self == %@", categoryId)
            
            let result = try managedContext.fetch(fetchRequest)
            
            if (result.count > 0)
            {
                let category = result[0]
                category.addToGroceries(grocery)
                try managedContext.save()
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    public func deleteGrocery(groceryId : NSManagedObjectID) {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            let fetchRequest = NSFetchRequest<Grocery>(entityName: "Grocery")
            
            fetchRequest.predicate = NSPredicate(format: "self == %@", groceryId)
            
            let result = try managedContext.fetch(fetchRequest)
            
            if (result.count > 0){
                let grocery = result[0]
                managedContext.delete(grocery)
                try managedContext.save()
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    public func editGrocery(name : String, date : NSDate, image : UIImage, groceryId : NSManagedObjectID, category : NSManagedObject) {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            let fetchRequest = NSFetchRequest<Grocery>(entityName: "Grocery")
            
            fetchRequest.predicate = NSPredicate(format: "self == %@", groceryId)
            
            let result = try managedContext.fetch(fetchRequest)
            
            if (result.count > 0)
            {
                let grocery = result[0]
                grocery.setValue(name, forKey: "name")
                grocery.setValue(date, forKey: "date")
                grocery.setValue(category, forKey: "category")
                let convertedImage = UIImageJPEGRepresentation(image, 1)
                grocery.setValue(convertedImage, forKey: "image")
                try managedContext.save()
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    public func getAllGroceries() -> [NSManagedObject]
    {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Grocery")
        
        let groceryCategory = NSSortDescriptor(key: "name", ascending: true);
        
        fetchRequest.sortDescriptors = [groceryCategory];
        
        //3
        do {
            return try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return []
    }
    
    public func getWantedExpiringGroceries(daysUntilExpireDate: Int16) -> [NSManagedObject] {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Grocery")
        
        let expireDate = Calendar.current.date(byAdding: .day, value: Int(daysUntilExpireDate), to: Date())! as NSDate
        
        fetchRequest.predicate = NSPredicate(format: "date < %@ AND bought == nil", expireDate)
        
        let groceryCategory = NSSortDescriptor(key: "name", ascending: true);
        
        fetchRequest.sortDescriptors = [groceryCategory];
        
        do {
            return try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return []
    }
    
    public func getGroceryById(groceryId : NSManagedObjectID) -> NSManagedObject {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            let fetchRequest = NSFetchRequest<Grocery>(entityName: "Grocery")
            
            fetchRequest.predicate = NSPredicate(format: "self == %@", groceryId)
            
            let result = try managedContext.fetch(fetchRequest)
            
            if (result.count > 0)
            {
                return result[0]
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return NSManagedObject()
    }
    
    public func getGroceryNumOfDaysLeft(groceryId : NSManagedObjectID) -> Int {
        let managedContext = appDelegate.persistentContainer.viewContext
        let calendar = NSCalendar.current
        let today = calendar.startOfDay(for: Date())
        
        do {
            let fetchRequest = NSFetchRequest<Grocery>(entityName: "Grocery")
            
            fetchRequest.predicate = NSPredicate(format: "self == %@", groceryId)
            
            let result = try managedContext.fetch(fetchRequest)
            
            if (result.count > 0){
                let grocery = result[0]
                let bestBeforeDate = calendar.startOfDay(for: grocery.date! as Date)
                
                return calendar.dateComponents([.day], from: today, to: bestBeforeDate).day!
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return 0
        
    }
    
    public func deleteAllGroceries() {
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Grocery")
        
        do {
            for grocery in try managedContext.fetch(fetchRequest) {
                managedContext.delete(grocery)
            }
            try managedContext.save()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    public func addBoughtGroceries(_ boughtGroceries: [NSManagedObject]){
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let groceries = NSSet(array: boughtGroceries)
        
        let entity = NSEntityDescription.entity(forEntityName: "BoughtGroceries", in: managedContext)!
        
        let list = NSManagedObject(entity: entity, insertInto: managedContext)
        list.setValue(groceries, forKey: "groceries")
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    public func getBoughtGroceries() -> [NSManagedObject]{
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do{
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "BoughtGroceries")
            
            let result = try managedContext.fetch(fetchRequest)
            var groceries: [NSManagedObject] = []
            
            for res in result {
                let list = res as! BoughtGroceries
                print(res)
                groceries.append(contentsOf: (list.groceries!.sortedArray(using: [NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSNumber.compare(_:)))]) as! [NSManagedObject]))
            }
            return groceries
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return []
    }
    
    public func deleteBoughtGrocery(_ boughtGroceryId: NSManagedObjectID){
        let managedContext = appDelegate.persistentContainer.viewContext
        print("ID", boughtGroceryId)
        do {
            let fetchRequest = NSFetchRequest<BoughtGroceries>(entityName: "BoughtGroceries")
            
            let result = try managedContext.fetch(fetchRequest)
            
            if (result.count > 0){
                for res in result {
                    for grocery in (res.groceries?.allObjects as! [NSManagedObject]) {
                        if grocery.objectID == boughtGroceryId {
                            res.removeFromGroceries((grocery as! Grocery))
                            try managedContext.save()
                        }
                    }
                }
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    public func deleteBoughtGroceries(){
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest =
            NSFetchRequest<BoughtGroceries>(entityName: "BoughtGroceries")
        
        do {
            for list in try managedContext.fetch(fetchRequest) {
                managedContext.delete(list)
            }
            try managedContext.save()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    public func getNumOfBoughtGroceries() -> Int{
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do{
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "BoughtGroceries")
            
            let result = try managedContext.fetch(fetchRequest)
            
            var numBought = 0
            for res in (result as! [BoughtGroceries]) {
                numBought += (res.groceries?.count)!
            }
            
            return numBought
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return 0
    }
}
